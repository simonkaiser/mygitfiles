<?php
	error_reporting(0);
	$id=$_GET['id'];
	require("categorydb.php");
	$display_qry=mysqli_query($con,"SELECT * FROM `category_table`");
		if ($id!="") 
		{
			$delete_qry=mysqli_query($con,"DELETE FROM `category_table` WHERE `id`='$id'");
			header('location:categorydisplay.php');
		}
?>

<html>
<head>
	<title>Display Category Details</title>
	<style>
		table {
   			border-collapse: collapse;
    		width: 50%;
			}

		th, td {
   			text-align: center;
    		padding: 8px;
			}

		tr:nth-child(odd){background-color: #f2f2f2}
			{
			color:rgb(128,0,0);
			}

		th {
    		background-color: rgb(128,0,0);
    		color: white;
			}
	</style>
</head>
	<body>
		<center>
			<table>
				<tr>
					<th>ID</th>
					<th>Category</th>
					<th>Parent ID</th>
					<th>Bread Crump</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>

<?php
	while($fetch=mysqli_fetch_assoc($display_qry)){
?>
				<tr>
					<td><?php echo $fetch['id'];?></td>
					<td><?php echo $fetch['category'];?></td>
					<td><?php echo $fetch['pid'];?></td>
					<td><?php echo $fetch['bc'];?></td>
					<td><a href="categoryedit.php?id=<?php echo $fetch['id'];?>">Edit</td>
					<td><a href="categorydisplay.php?id=<?php echo $fetch['id'];?>" onclick="return confirm('Are u sure want to delete this?')">Delete</td>
				</tr>
<?php
}
?>
			</table>
		</center>
	</body>
</html>